<h1>Crash Issue</h1>
**Quick questions**

OS:

HOI4 version:

DH version:

List any other mods used:

Were you using Steam?

Were you in multiplayer?

Which expansions do you NOT have?

**Explanation of the issue:**


**Steps to reproduce:**

1.

2.

**Other info:**


**Possible cause:**


**Log file:**
<!-- If you have the log file: zip it before you drag & drop it here. Both error log and game log are useful to us.-->

**Screenshots / Video:**
<!-- Drag & drop screenshots here. Use https://youtube.com to upload video. -->

**Save game:**
Attach a save from before the crash, ideally one where it always happens. A save file is necessary to be able to fix CTDs. Issues without saves will be closed.
