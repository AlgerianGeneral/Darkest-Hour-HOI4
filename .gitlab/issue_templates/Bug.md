<h1>Bug Issue</h1>
**Quick questions**

OS:

HOI4 version:

DH version:

List any other mods used:

Were you using Steam?

Were you in multiplayer?

Which expansions do you NOT have?

**Save file**


**Logs** (At least game and error log)

**Explanation of the issue:**


**Steps to reproduce:**

1.

2.

**Possible cause:**


**Screenshots:**

